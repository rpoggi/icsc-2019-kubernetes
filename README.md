# iCSC 2019 - Micro-services with Docker and Kubernetes

Hands-on exercise session for the iCSC 2019 lecture on "[How container orchestration can strengthen your micro-services: the approach of Kubernetes](https://indico.cern.ch/event/766995/contributions/3295781/)".

## :newspaper: Bulletin Board

:boom: You will be able to continue with the exercises until **Sunday 10th of March** at midnight.

:exclamation: After that, I suggest you to check the [admin](admin) directory which contains the commands and the instructions to setup your own Docker machine and your own Kubernetes cluster.

Feel free to email me at riccardo.poggi@cern.ch for any question or feedback you might have. As well if you encounter any issue open a merge request or send me an email.


## Setup
Follow the setup instructions in this section before starting with the exercises.

1. You have been assigned a student id (SID), which is a two digit number from `01` to `35`.

2. Ssh into the corresponding "docker" node `icsc-docker-<sid>`, with user `student-<sid>` and password `icsc2019`:
```bash
$ SID=<my two digit number student id>
$ ssh -Y student-$SID@icsc-docker-$SID
```

3. Now clone the exercise repository (available [here](https://gitlab.cern.ch/rpoggi/icsc-2019-kubernetes)):
```bash
$ git clone https://:@gitlab.cern.ch:8443/rpoggi/icsc-2019-kubernetes.git
```

4. Source the `setup.sh` script using the student id that has been given to you:
```bash
$ cd icsc-2019-kubernetes
$ source setup.sh <my two digit number student id>
```
If everything is setup correctly at the end you should get something like:
```bash
[...]
[INFO] Testing 'kubectl get node' command...
$ kubectl get node
NAME                                    STATUS    ROLES     AGE       VERSION
icsc-cluster-01-jjkyvo5lewtl-master-0   Ready     master    4d        v1.12.3
icsc-cluster-01-jjkyvo5lewtl-minion-0   Ready     <none>    4d        v1.12.3
icsc-cluster-01-jjkyvo5lewtl-minion-1   Ready     <none>    4d        v1.12.3
```
This is your Kubernetes cluster, at your disposal you have one master node and two minion nodes.

You are now ready to move to the exercise section, good luck and have fun!

:thumbsup: BTW the setup scripts enables `kubectl` auto-completion, which can be quite useful during the exercises.

## Exercises
The exercises are divided into two series: one focused on Docker and one focused on Kubernets. For each series exercises are ordered in an increasing level of difficulty. To warm up we will do the first one of each series together.

### Docker Series
- [ex-dock/01-hello-flask](ex-dock/01-hello-flask) - Docker hello world + Containerised Flask

### Kubernetes Series
`kubectl` is a command line interface for running commands against Kubernetes clusters. A cheatsheet is available online [here](https://kubernetes.io/docs/reference/kubectl/cheatsheet/).

- [ex-k8s/01-pod](ex-k8s/01-pod) - Play with K8s Pod
- [ex-k8s/02-replicaset](ex-k8s/02-replicaset) - Play with K8s Replicaset
- [ex-k8s/03-deployment](ex-k8s/03-deployment) - Play with K8s Deployment
- [ex-k8s/04-service](ex-k8s/04-service) - Play with K8s Service
- [ex-k8s/05-back-front-end](ex-k8s/05-back-front-end) - Communication between back- and front-end services
- [ex-k8s/06-parallel-processing](ex-k8s/06-parallel-processing) - Parallel processing
- [ex-k8s/07-ingress](ex-k8s/07-ingress) - Ingress fan-out


## Troubleshooting
- To access some of the services from your laptop browser you need to be connected on the CERN/eduroam network.

## Acknowledgement
Many thanks to the CSC organisers, mentors and the technical support. To the CERN IT Resource Provisioning Services. And to all those who helped and made this possible!

- Sebastian Lopienski
- Enric Tejedor
- Jan van Eldik
- Ricardo Rocha
- Luca Gardi
- Simone Mosciatti ([@siscia\_](https://twitter.com/siscia_))

## Support
Feel free to open an issue/merge-request or contact me for any question at: Riccardo.Poggi@cern.ch
