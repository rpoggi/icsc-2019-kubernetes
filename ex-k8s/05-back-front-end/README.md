# Exercise 05 - Connecting front-end and back-end

We were going to improve even more our dynamic message. Let's now have a back-end providing the message.

## 1. Implement a back-end and front-end communication

1. Edit `ex_05_frontend.py` to accept messages from the backend service and render them.
2. Edit the `Dockerfile`, you can specify the command from the Kubernetes deplpoyment description.
