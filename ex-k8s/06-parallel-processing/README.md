# Exercise 06 - Workers scalability for parallel processing

## 1. Create the infrastructure

1. Create Redis in HA:
```bash
$ kubectl create -f redis-master.yaml
$ kubectl create -f redis-slave.yaml
```

2. Build the web and worker docker images
  - Notice that the Dockerfile does not specifies the command, this is intended.
  - web and worker share the same docker image and the command is specified in their Deployment resource

3. Edit the `web.yaml` and `worker.yaml` with your image and create the web and workers Deployment and Services:
```bash
$ kubectl create -f web.yaml
$ kubectl create -f worker.yaml
```

4. Delete any previous ingress resource and create the one defined in this exercise:
```bash
$ kubectl create -f ex-06-ingress.yaml
```

5. Access the web page/interface. Use your assigned DNS alias:
  - Open your browser (you need to be connected on the CERN network)
  - Visit the following url: `icsc-cluster-xx.cern.ch`
  - Substitute `xx` with your student ID
  - From the webpage you can submit tasks: short, medium or long
  - Notice: the interface is not perfect, if you refresh the list will reset. This will not be important for this exercise.
  - Connect to: `icsc-cluster-xx.cern.ch/rq` to see the RedisQueue dashboard
  - Here you can check how many tasks are queued and how many and which workers are working (toggle button top right)

## 2. Scalability measurement
1. Open the Kubernetes dashboard, we will focus on the CPU usage graph
  - Alternatively you can use `kubectl top`
  - The refresh rate is ~1min (I know it is not ideal)
2. Now create load on your workers by submitting tasks
3. Check the CPU usage grow and plateau, note down this plateu value
4. Increase the number of workers and repeat point 2 and 3, i.e. apply a load and check the value of the CPU usage plateau
5. What happened? Were you able to overall use more CPU?
  - By how much?
  - Is this linear with the number of workers?
  - If you feel like you can create a scalability plot: on the x-axis the number of workers and on the y-axis the overall CPU usage. The plot will show how these two quantities scale to each other.
6. Will the overall CPU keep growing indefinitely?
  - What happened?
  - Check the Deployment configuration file, is the resource limit playing a role here?
  - Is it consistent with what you measured?
  - You can try playing with this value

## 3. Curing asymmetries

1. Create a few workers (don't go crazy, but not too few)
2. Mark one of the minion nodes for maintenance
3. Kill all the pods on that node (choose your method)
4. Now you should have all workers on one node and not the other
5. What could cure this asymmetry? If you don't find the solution feel free to send me an email ;)
