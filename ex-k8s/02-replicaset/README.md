# Exercise 02 - The ReplicaSets

The purpose of a ReplicaSet is to maintain a stable set of Pods running at any given time. In this exercise we'll explore its core concepts.


## 1. Create your own ReplicaSet
The ReplicaSet template specs looks a lot like the Pod definition before, but additionally you have `spec.replicas` which is the number of Pod you want running at any given time. It is the job of the ReplicaSet to make sure that this many replicas are running at all time and the way it finds these Pods is through label matching.

1. Edit the `ex-02-replicaset.yaml` file and create a ReplicaSet


## 2. Running replicas
1. What happens if you delete a Pod?

Useful commands:
```bash
$ kubectl get pods
$ kubectl get replicas
```

## 3. Scale up and down
This will be so easy to do with Kubernetes that to call it an exercise feels like an overstatement.

1. Use the `kubectl scale` command to increase and/or decrease the number of replicas.
  - You can use `kubectl get pods` to check that everything is behaving as supposed

## 4. Delete your ReplicaSet
1. Delete your ReplicaSet. What happend to the Pods that it managed?

## Limitation of a ReplicaSet
What you cannot do with a ReplicaSet is change the image inside the Pod template. So if you have a new version of your image that you want to rollout you will need a Deployment.
