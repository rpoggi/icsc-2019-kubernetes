import os
import platform

from flask import Flask
app = Flask(__name__)
app.env = 'development'


@app.route('/')
def root():
    msg = os.environ.get('MESSAGE', "env variable 'MESSAGE' not set")
    msg += '\nHostname: {}\n'.format(platform.node())
    msg += 'System: {}\n'.format(platform.platform())

    print('[INFO] Message: {}'.format(msg))

    return msg


if __name__ == '__main__':
    print('[INFO] Starting flask server app {}...'.format(app))
    print('[INFO] Message: {}'.format(os.environ.get('MESSAGE', "env variable 'MESSAGE' not set")))
    port = int(os.environ.get('PORT', 8080))
    app.run(host='0.0.0.0', port=port, debug=True)
    print('[INFO] Exiting...')
