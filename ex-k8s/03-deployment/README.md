# Exercise 03 - Finally we get to have fun!
Now that we have the basics it is time to have fun with deployments.

The resource description of a Deployment is basically identical to that of a ReplicaSet, but the `kind` is different, it is indeed: Deployment.


## 1. Complete the .yaml file and create a deployment

The resource description of a Deployment is basically identical to that of a ReplicaSet, but the type is Deployment. A deployment description file is already prepared for you, just remember to edit it to update the `spec.containers.image` field:
```bash
$ cat ex-03-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ex-03-server
spec:
  replicas: 4
  selector:
    matchLabels:
      app: ex-03-app
  template:
    metadata:
      labels:
        app: ex-03-app
    spec:
      containers:
      - name: ex-03-app-container
        image: <add here your own image>
        imagePullPolicy: Always
        env:
        - name: MESSAGE
          value: v1 of my message!
```

## 2. Rollout of a new deployment

1. Change the message from the Deployment resource description and rollout the new version. Useful commands:
  - `kubectl edit deployment xxx`
  - `kubectl rollout status`
  - `kubectl rollout history`

Moreover, to see the new version of your deployment propagating through your cluster you can use the following command:
```bash
$ kubectl logs --selector 'app=ex-03-app' | grep Message
```

## 3. Rollback to the previous version

Ops, maybe you realised that it was the wrong version.

1. Rollback to the previous version and see the changes propagate through your Kubernetes cluster.
  - Hint: check the slides

## 4. (optionl) Scale the deployment

1. Deployments inherit the same scaling feature of a ReplicaSet, if you want you can see it in action:
  - `kubectl scale`

## Conclusion
Take home message: differently from ReplicaSet Deployment have the notion of application _life-cycle_!
