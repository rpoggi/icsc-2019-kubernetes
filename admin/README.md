# Admin guide

## Setup

```bash
$ ssh lxplus-cloud.cern.ch
```
### First time
Go to https://openstack.cern.ch/project/ select your project (upper left) -> API Access -> Download openstack rc file

```bash
$ source .openrc
$ ssh-keygen -t rsa
$ openstack keypair create --public-key ~/.ssh/id_rsa.pub lxplus
```
### Second time
```bash
$ source .openrc
```

## Cluster management

Creating a kubernets cluster:

```bash
$ openstack coe cluster create icsc-cluster --keypair lxplus  --cluster-template kubernetes --node-count 2
```

Scaling a cluster
```bash
$ openstack coe cluster update icsc-cluster replace node_count=5
```

To use a cluster:
```bash
$ $(openstack coe cluster config icsc-cluster)
$ kubectl get node
NAME                                    STATUS    ROLES     AGE       VERSION
icsc-cluster-01-jjkyvo5lewtl-master-0   Ready     master    4d        v1.12.3
icsc-cluster-01-jjkyvo5lewtl-minion-0   Ready     <none>    4d        v1.12.3
icsc-cluster-01-jjkyvo5lewtl-minion-1   Ready     <none>    4d        v1.12.3
```

### Open dashboard
After starting the proxy:
```bash
$ kubectl proxy
```

The dashboard is then accessible by accessing (on your host): http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

```bash
$ kubectl -n kube-system get secret
```

```bash
$ kubectl -n kube-system describe secret admin-token-67tq5
```


## Ingress
Select two minion nodes to host Ingress. Do this by labelling them with `role=ingress`:

```bash
$ kubectl get node
NAME                                 STATUS    ROLES     AGE       VERSION
icsc-cluster-lkyovunoz4q2-minion-0   Ready     <none>    1d        v1.12.3
icsc-cluster-lkyovunoz4q2-minion-1   Ready     <none>    1d        v1.12.3

$ kubectl label node icsc-cluster-lkyovunoz4q2-minion-1 role=ingress
node "icsc-cluster-lkyovunoz4q2-minion-1" labeled

$ kubectl get nodes --show-labels
NAME                                 STATUS    ROLES     AGE       VERSION   LABELS
icsc-cluster-lkyovunoz4q2-minion-0   Ready     <none>    7d        v1.12.3   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=icsc-cluster-lkyovunoz4q2-minion-0
icsc-cluster-lkyovunoz4q2-minion-1   Ready     <none>    7d        v1.12.3   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=icsc-cluster-lkyovunoz4q2-minion-1,role=ingress

$ openstack server set --property landb-alias=icsc-cluster--load-1- icsc-cluster-lkyovunoz4q2-minion-0
$ openstack server set --property landb-alias=icsc-cluster--load-2- icsc-cluster-lkyovunoz4q2-minion-1
```

The landb alias set will then be accessible from within the CERN network.

## Ingress

```bash
$ cat ingress.yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
spec:
  backend:
    serviceName: my-service
    servicePort: 80
```

Useful commands:
```bash
$ kubectl create -f ingress.yaml
$ kubectl get ingress test-ingress
NAME           HOSTS     ADDRESS   PORTS     AGE
test-ingress   *                   80        1h
$ kubectl describe ingress test-ingress
Name:             test-ingress
Namespace:        default
Address:
Default backend:  my-service:80 (10.100.36.2:8080,10.100.36.3:8080,10.100.89.10:8080)
Rules:
  Host  Path  Backends
  ----  ----  --------
  *     *     my-service:80 (10.100.36.2:8080,10.100.36.3:8080,10.100.89.10:8080)
Annotations:
Events:  <none>
$ kubectl edit ingress test-ingress
```

## Docker
Create a CentOS-7 VM through openstack, then login in the machine and as root run the provisioning script [provision.sh](admin/docker_nodes/provision.sh):

```bash
$ cd admin/docker_nodes # you need to cd into this directory
$ sudo ./provision.sh
```

Basically what is necessary is (as sudo):
```bash
$ yum install -y docker git
$ yum install -y bash-completion bash-completion-extras
$ cp kubernetes.repo /etc/yum.repos.d/kubernetes.repo
$ yum install -y kubectl

$ yum groupinstall "X Window System"

$ groupadd docker
$ usermod -aG docker $USER
$ systemctl enable docker
$ systemctl start docker
```
